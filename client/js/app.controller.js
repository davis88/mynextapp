// THIS IS ANGULAR JS
"use strict";

(function () {
  "use strict";
  app.controller('BaseController', BaseController)

  // INJECTING $http
  BaseController.inject = ["$http"]

  function BaseController($http) {
    var self = this;
    self.loaded = true
  }

}
)()